<?xml version="1.0" encoding="utf-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">

	<xs:element name="DateTime">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="SourceTimeStamp" type="xs:string"/>
					<!-- GMT, format yyyy-mm-ddThh:mm:ssZ -->
				<xs:element name="InterfaceVersion" type="xs:string"/>
					<!-- Web service version -->
				<xs:element name="SystemSeqNumber" type="xs:integer"/>
					<!-- WSI-generated unique tracking number for the message -->
			</xs:sequence>
		</xs:complexType>
	</xs:element>


<xs:element name="AirlineCode">
	<xs:complexType>
		<xs:sequence>
			<xs:element name="CID" type="xs:string"/>
				<!-- WSI-assigned customer ID -->
			<xs:element name="IATA" type="xs:string"/>
				<!-- Airline IATA code -->
			<xs:element name="ICAO" type="xs:string"/>
				<!-- Airline ICAO code -->
		</xs:sequence>
	</xs:complexType>
</xs:element>


<xs:element name="FlightKey">
	<xs:complexType>
		<xs:sequence>
			<xs:element name="AirlineCode"/>
			<xs:element name="SchedDepDateTime" type="xs:string"/>
				<!-- GMT, format yyyy-mm-ddThh:mm:ssZ -->
			<xs:element name="SchedDepICAO" type="xs:string"/>
				<!-- ICAO code of departure airport -->
			<xs:element name="FlightNumber" type="xs:string"/>
				<!-- Numeric part of flight identifier -->
			<xs:element name="FlightLegInstanceNumber" type="xs:integer"/>
				<!-- Used to create unique key for multiple departures on same day -->
		</xs:sequence>
	</xs:complexType>
</xs:element>


<xs:element name="VCR_Params">
	<xs:complexType>
		<xs:sequence>
			<xs:element name="Frequency" type="xs:string" minOccurs="1"/>
				<!-- VHF voice contact frequency, e.g., 103.45 -->
		</xs:sequence>
	</xs:complexType>
</xs:element>

<xs:element name="DPR_Params">
	<xs:complexType>
		<xs:sequence>
			<xs:element name="DispatcherInitials" type="xs:string" 
				minOccurs="0"/>
				<!--Initials of dispatcher who is working on this flight-->
			<xs:element name="DispatcherRemarks" type="xs:string" 
			 	minOccurs="0"/>
				<!--Text to be added to uplink message.-->
			<xs:element name="AlternateStation" type="xs:string" minOccurs="1"/>
				<!-- Station diversion is requested to, e.g., KLAS -->
			<xs:element name="FAFFuel" type="xs:string" minOccurs="0"/>
				<!-- Final approach fix fuel, e.g., 53.3 -->
			<xs:element name="FAFTime" type="xs:string" minOccurs="0"/>
        <!-- GMT, format hhmm -->
			<xs:element name="OrigArrStation" type="xs:string" minOccurs="1"/>
				<!-- Original flight destination, e.g., KSEA -->
			<xs:element name="FuelBurnToAlt" type="xs:string" minOccurs="0"/>
				<!-- Est. fuel burn to alt. station, e.g., 12.3 -->
			<xs:element name="TimeEntered" type="xs:string" minOccurs="1"/>
				<!-- GMT, format hhmm -->
		</xs:sequence>
	</xs:complexType>
</xs:element>

<xs:element name="ACARSMessageRequest">
	<xs:complexType>
		<xs:sequence>
			<xs:element name="ACARSMessageUniqueID" type="xs:string"/>
				<!--WSI-assigned identifier for the message request-->
			<xs:element ref="FlightKey"/>
			<xs:element name="AircraftRegNum" type="xs:string" 
				nillable="false"/>
				<!--Aircraft Registration Number, for example: N305AA-->
      <xs:element name="OriginalArrivalICAO" type="xs:string" minOccurs="0"/>
			<xs:element name="DispDesk" type="xs:string"/>
				<!--Dispatcher desk name/ID that is assigned to this flight-->
			<xs:element name="DispatcherName" type="xs:string" minOccurs="0"/>
				<!--Name of the dispatcher who is working on this flight-->
			<xs:element name="MessageType" type="xs:string" nillable="false"/>
				<!--FTM - Free Text Message
				    HGZ - Howgozit uplink request
				    SBY - Standby uplink request
				    VCR - Voice contact uplink request
				    DPR - Diversion plan uplink request-->
			<xs:element name="PositionFuelScheduleInterval" type="xs:string" 
				minOccurs="0"/>
				<!--In seconds. Present for MessageType = PFS.-->
			<xs:element name="AckNeeded" type="xs:boolean" default="true" 
				minOccurs="0"/>
				<!--Boolean true or false indicating that an acknowledgment 
				  is needed from the aircraft crew.-->
			<xs:element name="ActivateSELCAL" type="xs:boolean" default="false" 
				minOccurs="0"/>
				<!--Applicable when the MessageType is FTM. Boolean true or 
				   false indicating that the SELCAL chime is to be activated 
				   in the cockpit. Note: This is to be used by Dispatch personnel 
				   to insure that the SELCAL chime is not set off during takeoff.
				-->
			<xs:element name="GroundStation" type="xs:string" minOccurs="0"/>
				<!--ACARS remote ground station that is nearest to the aircraft 
				   for transmitting the message-->
			<xs:element name="SideWrite" type="xs:boolean" minOccurs="0"/>
				<!--Applicable when message type is FTM. The message will be 
				   printed side-write (landscape mode) on the ACARS printer 
				   when this option is true-->
			<xs:element name="MessageText" type="xs:string" minOccurs="0"/>
				<!--Applicable when the MessageType is FTM. Text message 
				   that will be sent to the ACARS printer in the aircraft.-->
			<xs:element ref="VCR_Params" minOccurs="0"/>
				<!--Parameters included only for MessageType=VCR-->
			<xs:element ref="DPR_Params" minOccurs="0"/>
				<!--Parameters included only for MessageType=DPR-->
		</xs:sequence>
	</xs:complexType>
</xs:element>


<xs:element name="ACARSMessageResponse">
	<xs:complexType>
		<xs:sequence>
			<xs:element name="ACARSMessageUniqueID" type="xs:string"
				minOccurs="0"/>
				<!--WSI-assigned identifier for the message request-->
			<xs:element ref="FlightKey"/>
			<xs:element name="AircraftRegNum" type="xs:string"/>
				<!--Aircraft Registration Number example: N305AA-->
			<xs:element name="MessageStatus" type="xs:string"/>
				<!--SENT: indicating that the message has been sent to the 
				      aircraft 
				   ERR: indicating an error occured when sending the message-->
			<xs:element name="DispDesk" type="xs:string" minOccurs="0"/>
				<!--Dispatcher desk name/ID that is assigned to this flight-->
			<xs:element name="DispatcherName" type="xs:string" minOccurs="0"/>
				<!--Name of the dispatcher who is working on this flight-->
			<xs:element name="HostReturnCode" type="xs:string"/>
				<!--Return code from the host ACARS system-->
			<xs:element name="HostResponse" type="xs:string"/>
				<!--Descriptive result message derived/obtained from the host 
				   ACARS system-->
			<xs:element name="AckMsgNum" type="xs:string" minOccurs="0"/>
				<!--This value will be set if an acknowledgement from crew was 
				   needed as part of the original request.-->
			<xs:element name="Sender" type="xs:string"  minOccurs="0"/>
				<!--Sender identifier defined by customer ACARS system. Might
				  be Fusion, for example, if uplink message originated in the
				  Fusion system.-->
			<xs:element name="MessageID" type="xs:string"  minOccurs="0"/>
				<!--Unique identifier for the uplink message defined by 
				  customer ACARS system. The ACARSMessageResponse is sent
				  in response to this uplink message.-->
			<xs:element name="MessageType" type="xs:string"  minOccurs="0"/>
				<!--Types defined by customer ACARS system-->
			<xs:element name="MessageTypeName" type="xs:string"  minOccurs="0"/>
				<!--Display names defined by customer ACARS system-->
			<xs:element name="MessageText" type="xs:string" minOccurs="0"/>
				<!--Message text formatted as it should be displayed within the
				  Fusion user interface.-->
		</xs:sequence>
	</xs:complexType>
</xs:element>


<xs:element name="ACARSDownlinkMessage">
	<xs:complexType>
		<xs:sequence>
			<xs:element ref="FlightKey"/>
				<!--Contains the aircraft information-->
			<xs:element name="AircraftRegNum" type="xs:string"/>
				<!--Aircraft Registration Number, example: N305AA-->
			<xs:element name="MessageID" type="xs:string"  minOccurs="0"/>
				<!--Unique identifier defined by customer ACARS system-->
			<xs:element name="MessageType" type="xs:string"  minOccurs="0"/>
				<!--Types defined by customer ACARS system-->
			<xs:element name="MessageTypeName" type="xs:string"  minOccurs="0"/>
				<!--Display names defined by customer ACARS system-->
			<xs:element name="MessagePriority" type="xs:string"  minOccurs="0"/>
				<!--Priorities defined by customer ACARS system-->
			<xs:element name="MessageSource" type="xs:string"  minOccurs="0"/>
				<!--Sources defined by customer ACARS system, for example,
				  to distinguish between automatic and crew-initiated
				  messages.-->
			<xs:element name="AckMessageNum" type="xs:string" minOccurs="0"/>
				<!--Acknowledgement Number that uniquely identifies this 
				  message in the host ACARS system. This shall only be 
				  provided if the downlink message is intended as an 
				  acknowledgement to an uplink message. -->
			<xs:element name="MessageText" type="xs:string" minOccurs="0"/>
				<!--Text received in the message-->
		</xs:sequence>
	</xs:complexType>
</xs:element>


	<xs:element name="InsertACARSRequest">
		<xs:complexType>
			<xs:sequence>
				<xs:element ref="DateTime"/>
				<xs:element ref="ACARSMessageRequest"/>
			</xs:sequence>
		</xs:complexType>
	</xs:element>

	<xs:element name="InsertACARSResponse">
		<xs:complexType>
			<xs:sequence>
				<xs:element ref="DateTime"/>
				<xs:element ref="ACARSMessageResponse"/>
			</xs:sequence>
		</xs:complexType>
	</xs:element>

	<xs:element name="InsertACARSDownlinkMsg">
		<xs:complexType>
			<xs:sequence>
				<xs:element ref="DateTime"/>
				<xs:element ref="ACARSDownlinkMessage"/>
			</xs:sequence>
		</xs:complexType>
	</xs:element>

</xs:schema>